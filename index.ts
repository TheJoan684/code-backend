import express , { Express , Request, Response} from "express";
import dotenv from 'dotenv';
 // configuration the .env file
dotenv.config();

// Create Express APP

const app: Express = express();
const port: string | number = process.env.PORT || 8000;


//define the first Route

app.get('/' , (req: Request,res:Response) => {
	//send a hello World
	res.send('Welcome to @JoanG :)');
});

// Execute APP
app.listen(port,() =>{
	console.log(`Express Sever Running  at http://localhost:${port}`)
})
